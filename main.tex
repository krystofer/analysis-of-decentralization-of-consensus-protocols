\documentclass[conference]{IEEEtran}

%\documentclass{paper}

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{epsfig} % for postscript graphics files
\usepackage{mathptmx} % assumes new font selection scheme installed
\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{amssymb}  % assumes amsmath package installed
\newtheorem{theorem}{Theorem}[section]

\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{hyperref}


\title{Analysis of decentralization of typical blockchain consensus algorithms with differential equations}

\begin{document}
\maketitle

\begin{abstract}
Blockchain and underlying consensus algorithms recently have received a significant attention from the point of view of both game theory and computer science. The key property of blockchain based cryptocurrencies is censorship resistance. Censorship resistance is achieved by decentralization of operations. Interplay of multiple agents in the system makes its behavior nonlinear and inherently complex. At the same time multi-agency and time-dependence of existing consensuses creates possibility to examine them using dynamical systems. Dynamical systems perspective allows to study not only economic incentives in consensus ecosystems but the whole space of possible behaviors, strategies and outcomes. In this paper we define sets of differential equations that describe behavior of common consensus algorithms: proof of work (PoW), proof of stake (PoS) and delegated proof of stake (dPoS). We study solutions of these equations and compare their properties with respect to robustness of decentralization. In particular, we study whether one agent in a system can centralize power by reinvesting profits from mining or minting into hashing power or stake. We show that PoW and PoS demonstrates a constant ratio between hashing power or stakes maintains between agents that reinvest profits irrespective of initial conditions. dPoS demonstrates divergence of stakes of two nodes that reinvest profits, leading to eventual strong monopolisation.
\end{abstract}

%---------------------------------------------------------------------------------------------
\section{Introduction}

Blockchain field, which appeared within the last decade due to success of the Bitcoin network \cite{bitcoin}, has brought a promise of trustless decentralisation as a solution for a number of social and industrial problems, like single points of failure, misaligned trust, conflicts of interest and information malformation \cite{bitcoin, ethereum}. Consensus algorithms allow independent multiple parties to come to the same conclusion in parallel mode without any central authority. Thus, proper robust consensus algorithms are the keystone for decentralization of cryptocurrencies. 

Formal analysis of consensus algorithms are still in its early days. As of today, most of the consensuses were created in \emph{ad hoc} empirical manner, without pre-modeling or any other formal verifications. The only known exception is Ouroboros consensus algorithm \cite{ouroboros}, \cite{ouroboros-praos}). \emph{Post hoc} they were analysed in some studies with game theory models and for byzantine fault tolerance \cite{bitcoin-backbone}, \cite{pos-bft}. However this is still far from analysis that may allow to formally predict their properties.

Research on byzantine fault tolerance in distributed systems is an early attempt to create decentralised networks that will be resilient to different forms of information loss or change \cite{dls}, \cite{pbft}. Results of byzantine fault tolerance research are used by modern proof-of-stake algorithms \cite{tendermint}, \cite{polkadot}, however they are suited only for analysing algorithm resistance to different types of attacks.

Methods of game theory, traditionally applied to the topic of blockchain consensuses, allow to investigate economically-beneficial strategies, like different forms of Nash equilibrium \textbf{[citation needed]}. However they are poorly suited for exploring all possible behaviour strategies within consensus, as well as finding different forms of equilibrium caused by the system evolution in case it is not attributed to rational decisions of the agents. 

The key property of cryptocurrencies is censorship resistance. Censorship resistance is achieved by decentralization of operations. That is, agents independently mine in proof of work algorithms or mint new coins, validate transactions etc. Interplay of multiple agents in the system makes its behavior nonlinear and inherently complex. Dynamical systems theory is a mathematical discipline that is often used to study systems with many interacting agents with the use of differential equations. Dynamical systems perspective allows to study not only economic incentives in consensus ecosystems but the whole space of possible behaviors, strategies and outcomes.

In this paper we have selected three typical consensus algorithms and explored their properties with respect to decentralization using differential equations. The selected algorithms are proof of work (PoW), proof of stake (PoS) and delegated proof of stake (dPoS). We study solutions of these equations and compare their properties with respect to robustness of decentralization. In particular, we study whether one agent in a system can centralize power by reinvesting profits from mining or minting into hashing power or stake. 

%---------------------------------------------------------------------------------------------

\section{Proof of work}
Proof of work (PoW), originally proposed by Adam Back in Hashcash algorithm \cite{hashcash}, was widely adopted via Bitcoin consensus algorithm and its different forks and modifications \cite{bitcoin}. In the proof of work algorithm miners solve a cryptographic puzzle (finding a certain hash) in order to obtain the right to sign a new block and receive a mining reward \cite{bitcoin}. Proof of work algorithm provides a way to prevent double spendings, Syblic attacks and other types of attacks in decentralized cryptocurrency systems \cite{bitcoin}. Miners use their \textit{stake} \textit{S} (e.g., bitcoins) to invest into a specialized mining equipment (e.g., ASIC chips or GPU) to increase the so called hashing power (hash rate) \textit{H}, to have a higher chance to receive mining reward. Since the task of finding the right hash to sign the block is a competitive task between all miners \textit{n} in the network, the average reward for a miner \textit{i} is proportional to his relative hash rate, $pH_{ri}$, where $p$ is constant and 
\begin{equation}
H_{ri}=\frac{H_i}{\sum_{k=1}^n{H_k}}.
\label{eq:h_i}
\end{equation}

Miners also need to pay for electricity proportionally to theirs hash rate, $cH_i$. Here $c$ is the price of electricity to generate one hash. Combining this we can write the differential equation for the change of stake of miner \textit{i} 

\begin{equation}
\frac{dS_i}{dt} = pH_{ri}-cH_i= p \frac{H_i}{\sum_{k=1}^n{H_k}} - cH_i. 
\label{eq:s_i}
\end{equation}

Assuming that the price of new mining equipment per hash is $h$, the change of miner's \textit{i} hash rate can be described as 

\begin{equation}
\frac{dH_i}{dt} = \frac{I(S_i)}{h},
\label{eq:dh}
\end{equation}

where $I(S_i)$ is a function of investment. Note that the function of investment $I(S_i)$ can take negative values. Since we are interested in decentralization, in this case of hash rate between different miners, let us assume that miners act as adversaries and aim to increase their relative hash rate. To do so they reinvest all their mining reward into a new mining equipment. That is, $I(S_i)=\frac{dS_i}{dt}$. Therefore, the behavior of miner \textit{i} in the network with proof of work algorithm can be described with the following system of differential equations:


\begin{equation}
\begin{aligned}
\frac{dS_i}{dt} & = p \frac{H_i}{\sum_{k=1}^n{H_k}} - cH_i\\
\frac{dH_i}{dt} & = \frac{1}{h}\frac{dS_i}{dt}.
\end{aligned}
\label{eq:pow}
\end{equation}

The change in hash rate of miner \textit{i} can be presented as

\begin{equation}
\begin{aligned}
\frac{dH_i}{dt} & = \frac{1}{h}(p \frac{H_i}{\sum_{k=1}^n{H_k}} - cH_i)\\
& = \frac{H_i}{h}(\frac{p}{\sum_{k=1}^n{H_k}}-c).
\end{aligned}
\label{eq:abs_hash}
\end{equation}

The solution to the system of differential equations of $n$ miners with initial hash rate of $i$-th agent $H_i(0)$ is 

\begin{equation}
H_i = H_i(0)e^\frac{-ct}{h}\frac{cH_T(0)+p(e^\frac{ct}{h}-1)}{cH_T(0)},
\label{eq:ahash_sol}
\end{equation}

where $H_T(0)=\sum_{k=1}^{n}H_k(0)$ is the initial total hash in the network of \textit{n} agents. 

Consider two miners $i$ and $j$ competing to increase their relative hashing power by reinvesting all their profits into buying new hashing power. Let us calculate their relative hashing power. Using the equation (\ref{eq:ahash_sol}) it is easy to see that the fraction of hash rates of miners \textit{i} and \textit{j} in such system remains constant over time. 

\begin{equation}
\frac{H_i(t)}{H_j(t)} = \frac{H_i(0)}{H_j(0)} = const.
\label{eq:relhash}
\end{equation}

%------------------------------------------------------------------------------------------------------------------------------

\section{Proof of stake}
The concept of proof of stake was created to solve the problem of excessive energy consumption in proof of work consensus cryptocurrencies through the process of mining. Proof of stake algorithm was discussed in bitcointalk forum in 2011 and formally introduced by Sunny King (a pseudonym) and Scott Nadal in 2012 in Peercoin whitepaper \cite{peercoin}. However, Peercoin has a hybrid proof of work/proof of stake consensus \cite{peercoin}. 

In contrast to proof of work cryptocurrencies in proof of stake, a new block validator is chosen deterministically based on users’ stakes of coins. In a simple proof of stake system (like NXT, \cite{nxt}) every holder of stake of coins \textit{S} has a chance of signing of new block proportional to the stake of the owner \begin{math}pS\end{math}. Here $p>1$ is the rate of agent's stake increase, equal to the block reward. The change of stake for an agent \textit{i} can be described as

\begin{equation}
\frac{dS_i}{dt} = pS_i
\label{eq:pos}
\end{equation}

Assuming the initial stake of the agent \textit{i} is $S_i(0)$, the solution to the equation (\ref{eq:pos}) is given by

\begin{equation}
S_i = e^{pt+lnS_i(0)}.
\label{eq:S_final}
\end{equation}

From the equation (\ref{eq:S_final}) and taking into account \begin{math}p=const\end{math} it follows that the ratio of stakes between any agents \textit{i} and \textit{j} in a PoS system at any point of time stays constant:

\begin{equation}
\frac{S_i(t)}{S_j(t)} = \frac{S_i(0)}{S_j(0)} = const.
\label{eq:S_frac}
\end{equation}

%-----------------------------------------------------------------------------------------------------------------

\section{Delegated proof of stake}

With delegated proof of stake blocks are created by a small subset of nodes (called \emph{witnesses}). Witnesses receive reward $p$ for block creation \cite{bitshares}. Witnesses are selected through voting process. In a basic dPoS setting we can assume voting power in electing witnesses is proportional to the relative stake of an agent of the system of $n$ agents, thus the probability $\pi(s_i)$ to of a node $s_i$ to be elected as a witness is given by the following equation:

\begin{equation}
\pi(S_i)=\frac{S_i}{\sum_{k=1}^nS_k}.
\label{eq:pi}
\end{equation}. 

Given this probability function, the change of the stake of $i$-th agent with a time will be defined by

\begin{equation}
\frac{dS_i}{dt} = p \pi(S_i). 
\label{eq:dpos0}
\end{equation}

Taken together (\ref{eq:pi}) and (\ref{eq:dpos0}) give
\begin{equation}
\frac{dS_i}{dt} = p\frac{S_i}{\sum_{k=1}^nS_k}.
\label{eq:dpos}
\end{equation}

The solution to the system of differential equations of $n$ agents with initial stake of $i$-th agent $s_i(0)$ is 

\begin{equation}
S_i = S_i(0) + \frac{pt}{\sum_{k=1}^nS_k(0)}.
\label{eq:dpos_sol}
\end{equation}

Defining total stake in a system as $Z(t)$ we have \begin{equation}
Z(t) = \sum_{k=1}^nS_k(t) = \sum_{k=1}^nS_k(0) + pt = Z(0) + pt
\label{eq:Z}
\end{equation}. 

Let us introduce intermediate constant $\zeta = \frac{p}{Z(0)}$, representing block reward to initial stakes ratio. It follows that the ratio of stakes between any agents $i$ and $j$ in a dPoS system will hyperbolically diverge (if the initial stakes distribution was unequal $S_i(0) \neq S_j(0)$):

\begin{equation}
\frac{S_i(t)}{S_j(t)} = \frac{S_i(0) + \zeta t}{S_j(0) + \zeta t}.
\label{eq:dpos_ratio}
\end{equation}

The hyperbolic divergence from equation (\ref{eq:dpos_ratio}) is shown in Fig.~\ref{fig:dpos_stakes}.

\begin{figure}[h]
\includegraphics[width=0.45\textwidth]{dPoS_Stakes.pdf}
\caption{Visualisation of stakes divergence $S_i(t)/S_j(t)$ with time in dPoS consensus for given two nodes with initial stakes $S_i(0)=1$ and $S_j(0)=2$ and $\zeta=1$.}
\label{fig:dpos_stakes}
\end{figure}

The scenario above corresponds to the case when everyone votes for themselves to become a witness. 
To get votes from other nodes one needs to have a high reputation. For simplicity, we assume that reputation correlates with the relative stakes $r_i \in R$ of the nodes in a dPoS network:
%(since from the game theory model we know that a better strategy is to vote for those who can be elected due to their high stake): - reference?

\begin{equation}
r_i = \frac{S_i}{\sum_{k=1}^ns_k} = \frac{S_i}{Z}.
\label{eq:rel}
\end{equation}

We assume that a node $i$ gets a fraction of votes $v_i$ from all the other stakes of other nodes based on its relative stake:

\begin{equation}
v_i = r_i (Z - S_i) =\frac{s_i}{Z}(Z - S_i) = S_i - \frac{1}{Z}S^2_i.
\label{eq:votes}
\end{equation}

The probability of a node $i$ to be elected as a witness in this scenario becomes 
\begin{equation}
\pi(S_i)=\frac{S_i+v_i}{Z}.
\label{eq:pi2}
\end{equation}  

Combining equations (\ref{eq:dpos0}), (\ref{eq:votes}) with (\ref{eq:pi2}) we get a generic form of a differential equation for the change of stake:

\begin{equation}
\frac{dS_i}{dt} = p(\frac{2S}{Z} - \frac{S_i^2}{Z^2}) = pS_i\frac{2Z - S_i}{Z^2}
\label{eq:dPoS1}
\end{equation}

Making time explicit using (\ref{eq:Z}) we get the final form of a differential equation for dPoS stakes evolution:

\begin{equation}
\frac{dS_i}{dt} = pS\frac{2(Z(0)+pt)-S_i}{(Z(0)+pt)^2}.
\label{eq:dPoS_full}
\end{equation}

Solution of equation (\ref{eq:dPoS_full}) is
\begin{equation}
S_i = S_i(0)\frac{(Z(0)+pt)^2}{Z^2(0) +ptS_i(0)}.
\end{equation}

The ratio between stakes for agents $i$-th and $j$-th is
\begin{equation}
\frac{S_i(t)}{S_j(t)} = \frac{Z^2(0) + ptS_j(0)}{Z^2(0) + ptS_i(0)},
\end{equation}

which again gives hyperbolic divergence of stakes as for the simple dPoS equation (\ref{eq:dpos_ratio}) shown on Fig.~\ref{fig:dpos_stakes}.

In Appendix A we present a different proof of divergence of stakes in a dPoS by calculating the change of relative stakes. 

%-----------------------------------------------------------------------------------------------
\section{Discussion}

Decentralization is the key property of cryptocurrencies. In particular, decentralization is required for censorship resistance. Decentralization requirement poses problems for reaching consensus between agents because there is no centralized source of truth of events. For example, agreed true sequence of transactions is required to prevent double spends. Various consensus algorithms were invented that allow agents in a cryptocurrency network to come to an agreement, such as proof of work, proof of stake, delegate proof of stake, proof of reputation and others. We studied three main types of consensus algorithms commonly used in modern cryptocurrencies: proof of work, proof of stake and delegated proof of stake from the point of view of dynamical systems. 

One possible threat to decentralization is the accumulation of majority of mining hash rate or stakes in one hands. This can be achieved for example by reinvesting mining or minting profits into buying new hashing equipment in PoW systems or increasing stakes in PoS systems.  Our research shows that in proof of work consensus algorithm reinvestment of mining profits into hash rate does not lead to centralization of hash rate, given that at least some other miners do the same. Proof-of-stake consensus algorithm exhibits very basic behaviour with an exponential increase in all stakes still maintaining constant rations between stakes of all mining subjects. Therefore, proof-of-stake consensus algorithm is also not susceptible to centralization by staking profits from minting. On the other hand, delegated proof of stake consensus algorithm demonstrates divergence of stakes of agents. Thus, dPoS leads to the emergence of oligopoly and centralization of power in hands of agents with initially large stakes compared to the stakes of other agents. Our dPoS model does not include explicit model of reputation, which is not only determined by a size of agent's stake but also its behavior in ecosystem. Future models of interaction of agents in dPoS ecosystems should involve reputation as an independent variable. 

To the best of our knowledge, this is the first approach of the analysis of cryptocurrency ecosystems with common consensus algorithms from the point of view of dynamical systems. In such ecosystems complex and emergent behavior can emerge, such as phase transitions or chaos. In other areas of science such complex types of behavior have been successfully explained with the use of models with differential equations.  With this work we propose a holistic approach to cryptocurrency ecosystems, in which not only cryptography and technological aspects are considered but also the behavior and interactions of humans as active agents. We created mathematically simple yet insightful models of common consensus algorithms, which can help us to understand their strengths and weaknesses from the points of view of interacting agents.

Future research should focus on more detailed models of cryptocurrency ecosystems as systems of interacting agents. Models should involve varying costs of electricity between agents that are involved in mining, mining difficulty adjustment in PoW based cryptocurrencies, reputation based on agents behaviour, economics between agents, adversarial behavior, such as 51\% attacks. 

\begin{thebibliography}{9}

\bibitem{bitcoin}
  S. Nakamoto,
  \textit{Bitcoin: A peer-to-peer electronic cash system},
  %Addison Wesley, Massachusetts,
  %2nd edition,
  2008.
 
\bibitem{peercoin}
  S. King, S. Nadal,
  \textit{PPCoin: Peer-to-Peer Crypto-Currency with Proof-of-Stake},
  2012.
  
\bibitem{hashcash}
  A. Back,
  \textit{Hashcash - A Denial of Service Counter-Measure},
  2002.

\bibitem{nxt}
  Nxt community,
  \textit{Nxt Whitepaper},
  2014.

\bibitem{bitcoin-backbone}
  A. Garay, A. Kiayias, N. Leonardos,
  \textit{The Bitcoin Backbone Protocol: Analysis and Applications},
  2017.

\bibitem{dls}
  C. Dwork, N. Lynch, L. Stockmeyer,
  \textit{Consensusin the Presence of Partial Synchrony},
  Journal of the Association for Computing Machinery, Vol. 35, No. 2, pp.288-323,
  1988.

\bibitem{pbft}
  M. Castro, B. Liskov,
  \textit{Practical Byzantine Fault Tolerance},
  Proceedings of the Third Symposium on Operating Systems Design and Implementation, New Orleans, USA, February 
  1999.

\bibitem{pos-bft}
  I. Bentov, A. Gabizon, A. Mizrahi,
  \textit{Cryptocurrencies without Proof of Work},
  arXiv:1406.5694v9,
  2017.
  
\bibitem{bitshares}
  \textit{Delegated Proof-of-Stake Consensus}
  https://bitshares.org/technology/delegated-proof-of-stake-consensus/

\bibitem{ethereum}
  V. Buterin,
  \textit{Ethereum White Paper},
  https://github.com/ethereum/wiki/wiki/White-Paper

\bibitem{tendermint}
  J. Kwon,
  \textit{Tendermint: Consensus without mining},
  2014.

\bibitem{ouroboros}
  A. Kiayias, A. Russell, B. David, R. Oliynykov,
  \textit{Ouroboros: A provably secure proof-of-stake blockchain protocol},
  2017.
  
\bibitem{ouroboros-praos}
  B. David, P.Gazi, A. Russell, A. Kiayias,
  \textit{Ouroboros Praos: An adaptively-secure, semi-synchronous proof-of-stake blockchain},
  2017.
 
\bibitem{polkadot}
  G. Wood,
  \textit{Polkadot: Vision for a heterogeneous multi-chain framework},
  2017.


\end{thebibliography}

\section*{Appendix A}

In this Appendix we prove convergence of relative stake of an agent with a bigger initial stake of two agents in a dPoS system to 1. 

The differential equation (\ref{eq:dPoS_full}) can be simplified by replacing node stakes $S$ with relative stakes $R$ according to (\ref{eq:rel}):

\begin{equation}
\frac{dS}{dt} = p(2R - R^2) = pR(2 - R)
\label{eq:dpos_ri2}
\end{equation}

Equation for the relative stake can be expressed in explicit time-dependent form as

\begin{equation}
R=\frac{S}{Z}=\frac{S}{Z(0)+pt}.
\label{eq:ri}
\end{equation}
and from it we can directly express absolute stake $S$ as

\begin{equation}
S=(Z(0) + pt)R.
\label{eq:si}
\end{equation}

By differentiating it we get

\begin{equation}
\frac{dS}{dt}=\frac{d}{dt}((Z(0)+pt)R) = p(\frac{dR}{dt}t+R).
\label{eq:si_ri}
\end{equation}

Now from (\ref{eq:dpos_ri2}) and (\ref{eq:si_ri}) we can get a differential equation for the relative stake in dPoS system:

\begin{equation}
p(\frac{dR}{dt}t+R)=pR(2-R).
\end{equation}

Finally,

\begin{equation}
\frac{dR}{dt}=\frac{R}{t}(1-R).
\label{eq:dr}
\end{equation}

The solution of the equation (\ref{eq:dr}) is 

\begin{equation}
R=\frac{t}{C+t},
\label{eq:dpos_sol}
\end{equation}
where $C$ is a constant of integration. This equation demonstrates that the relative stake in dPoS system generally converges to unity.

\end{document}
